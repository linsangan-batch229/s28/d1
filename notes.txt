db.users.insertOne({
        "userName" : "Kim",
        "passsword" : "once1234"
    
})

db.users.insertOne({
        "userName" : "pares",
        "passsword" : "pares1234"
    
    
})


db.users.insertMany([
    {
        "username" : "pablo123",
        "password" : "1234"
    },
    {
        "username" : "pedro1234",
        "password" : "123455"
    },

])


//db.users.find(); - find all users
//db.products.find() - find all prod

db.products.find()
        - all
db.users.find({"userName" : "pares"})
        - specific

db.cars.find()
db.cars.find({type: "sedan"})

//find() - gets all documents that matches the critera


db.cars.findOne({})
        - find the first document that maches the criteria

db.cars.findOne({brand: "toyota"})
        - find the first specific


--------UPDATED

db.users.updateOne({userName: "pares"}, {$set:{userName: "siomai"}})
        - allows us update the first item that matches our criteria


db.users.updateOne({}, {$set: {username: "Shawarma"}})
        - update hte first item that matches our criteria, even though
        no criter

db.users.updateOne({username: "pablo123"}, {$set: {isAdmin:true}})
        - {find critera} {}
        - one item only

db.users.updateMany({}, {$set: {isAdmin:true}})
        - update lahat kahit walang criteria
        - lahat ng docs malalagyan ng isAdmin: true if wala pa,if meron magiging true

db.cars.updateMany({type: "sedan"}, {$set : {price: 100000}})
        - matches 2 sedan and update that 2 sedan


*******DELETE

db.users.deleteOne({})
        - delete the first item on 'users' collection tha m

db.cars.deleteOne({brand: "toyota"})
        - delete the frist item on collection that matches the criteria

db.users.deleteMany({isAdmin:true})
        -delete all item that matches the criteria

db.cars.deleteMany({})
        -   delete all cars collection

-----

